gdjs.MoonCode = {};
gdjs.MoonCode.localVariables = [];
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects1= [];
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects2= [];
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects3= [];
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects4= [];
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects5= [];
gdjs.MoonCode.GDMoon_9595UIObjects1= [];
gdjs.MoonCode.GDMoon_9595UIObjects2= [];
gdjs.MoonCode.GDMoon_9595UIObjects3= [];
gdjs.MoonCode.GDMoon_9595UIObjects4= [];
gdjs.MoonCode.GDMoon_9595UIObjects5= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects1= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects4= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects5= [];
gdjs.MoonCode.GDDogeButtonObjects1= [];
gdjs.MoonCode.GDDogeButtonObjects2= [];
gdjs.MoonCode.GDDogeButtonObjects3= [];
gdjs.MoonCode.GDDogeButtonObjects4= [];
gdjs.MoonCode.GDDogeButtonObjects5= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects1= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects4= [];
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects5= [];
gdjs.MoonCode.GDTip_9595Me_9595Objects1= [];
gdjs.MoonCode.GDTip_9595Me_9595Objects2= [];
gdjs.MoonCode.GDTip_9595Me_9595Objects3= [];
gdjs.MoonCode.GDTip_9595Me_9595Objects4= [];
gdjs.MoonCode.GDTip_9595Me_9595Objects5= [];
gdjs.MoonCode.GDDogeCoin_9595LogoObjects1= [];
gdjs.MoonCode.GDDogeCoin_9595LogoObjects2= [];
gdjs.MoonCode.GDDogeCoin_9595LogoObjects3= [];
gdjs.MoonCode.GDDogeCoin_9595LogoObjects4= [];
gdjs.MoonCode.GDDogeCoin_9595LogoObjects5= [];
gdjs.MoonCode.GDDogeCoin_9595AmountObjects1= [];
gdjs.MoonCode.GDDogeCoin_9595AmountObjects2= [];
gdjs.MoonCode.GDDogeCoin_9595AmountObjects3= [];
gdjs.MoonCode.GDDogeCoin_9595AmountObjects4= [];
gdjs.MoonCode.GDDogeCoin_9595AmountObjects5= [];
gdjs.MoonCode.GDDogeCoinsObjects1= [];
gdjs.MoonCode.GDDogeCoinsObjects2= [];
gdjs.MoonCode.GDDogeCoinsObjects3= [];
gdjs.MoonCode.GDDogeCoinsObjects4= [];
gdjs.MoonCode.GDDogeCoinsObjects5= [];
gdjs.MoonCode.GDDPS_9595LogoObjects1= [];
gdjs.MoonCode.GDDPS_9595LogoObjects2= [];
gdjs.MoonCode.GDDPS_9595LogoObjects3= [];
gdjs.MoonCode.GDDPS_9595LogoObjects4= [];
gdjs.MoonCode.GDDPS_9595LogoObjects5= [];
gdjs.MoonCode.GDTotal_9595DPSObjects1= [];
gdjs.MoonCode.GDTotal_9595DPSObjects2= [];
gdjs.MoonCode.GDTotal_9595DPSObjects3= [];
gdjs.MoonCode.GDTotal_9595DPSObjects4= [];
gdjs.MoonCode.GDTotal_9595DPSObjects5= [];
gdjs.MoonCode.GDDPS_9595AmountObjects1= [];
gdjs.MoonCode.GDDPS_9595AmountObjects2= [];
gdjs.MoonCode.GDDPS_9595AmountObjects3= [];
gdjs.MoonCode.GDDPS_9595AmountObjects4= [];
gdjs.MoonCode.GDDPS_9595AmountObjects5= [];
gdjs.MoonCode.GDMoonShopUIObjects1= [];
gdjs.MoonCode.GDMoonShopUIObjects2= [];
gdjs.MoonCode.GDMoonShopUIObjects3= [];
gdjs.MoonCode.GDMoonShopUIObjects4= [];
gdjs.MoonCode.GDMoonShopUIObjects5= [];
gdjs.MoonCode.GDUpgrade_9595ButtonObjects1= [];
gdjs.MoonCode.GDUpgrade_9595ButtonObjects2= [];
gdjs.MoonCode.GDUpgrade_9595ButtonObjects3= [];
gdjs.MoonCode.GDUpgrade_9595ButtonObjects4= [];
gdjs.MoonCode.GDUpgrade_9595ButtonObjects5= [];
gdjs.MoonCode.GDHelpers_9595ButtonObjects1= [];
gdjs.MoonCode.GDHelpers_9595ButtonObjects2= [];
gdjs.MoonCode.GDHelpers_9595ButtonObjects3= [];
gdjs.MoonCode.GDHelpers_9595ButtonObjects4= [];
gdjs.MoonCode.GDHelpers_9595ButtonObjects5= [];
gdjs.MoonCode.GDReddit_9595ButtonObjects1= [];
gdjs.MoonCode.GDReddit_9595ButtonObjects2= [];
gdjs.MoonCode.GDReddit_9595ButtonObjects3= [];
gdjs.MoonCode.GDReddit_9595ButtonObjects4= [];
gdjs.MoonCode.GDReddit_9595ButtonObjects5= [];
gdjs.MoonCode.GDReddit_9595LogoObjects1= [];
gdjs.MoonCode.GDReddit_9595LogoObjects2= [];
gdjs.MoonCode.GDReddit_9595LogoObjects3= [];
gdjs.MoonCode.GDReddit_9595LogoObjects4= [];
gdjs.MoonCode.GDReddit_9595LogoObjects5= [];
gdjs.MoonCode.GDTip_9595ButtonObjects1= [];
gdjs.MoonCode.GDTip_9595ButtonObjects2= [];
gdjs.MoonCode.GDTip_9595ButtonObjects3= [];
gdjs.MoonCode.GDTip_9595ButtonObjects4= [];
gdjs.MoonCode.GDTip_9595ButtonObjects5= [];
gdjs.MoonCode.GDTip_9595Me_9595TextObjects1= [];
gdjs.MoonCode.GDTip_9595Me_9595TextObjects2= [];
gdjs.MoonCode.GDTip_9595Me_9595TextObjects3= [];
gdjs.MoonCode.GDTip_9595Me_9595TextObjects4= [];
gdjs.MoonCode.GDTip_9595Me_9595TextObjects5= [];
gdjs.MoonCode.GDDiscord_9595ButtonObjects1= [];
gdjs.MoonCode.GDDiscord_9595ButtonObjects2= [];
gdjs.MoonCode.GDDiscord_9595ButtonObjects3= [];
gdjs.MoonCode.GDDiscord_9595ButtonObjects4= [];
gdjs.MoonCode.GDDiscord_9595ButtonObjects5= [];
gdjs.MoonCode.GDDiscord_9595LogoObjects1= [];
gdjs.MoonCode.GDDiscord_9595LogoObjects2= [];
gdjs.MoonCode.GDDiscord_9595LogoObjects3= [];
gdjs.MoonCode.GDDiscord_9595LogoObjects4= [];
gdjs.MoonCode.GDDiscord_9595LogoObjects5= [];
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects1= [];
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2= [];
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects3= [];
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects4= [];
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects5= [];
gdjs.MoonCode.GDSteamGroup_9595LogoObjects1= [];
gdjs.MoonCode.GDSteamGroup_9595LogoObjects2= [];
gdjs.MoonCode.GDSteamGroup_9595LogoObjects3= [];
gdjs.MoonCode.GDSteamGroup_9595LogoObjects4= [];
gdjs.MoonCode.GDSteamGroup_9595LogoObjects5= [];
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects1= [];
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2= [];
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects3= [];
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects4= [];
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects5= [];
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects1= [];
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects2= [];
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects3= [];
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects4= [];
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects5= [];
gdjs.MoonCode.GDGameJolt_9595ButtonObjects1= [];
gdjs.MoonCode.GDGameJolt_9595ButtonObjects2= [];
gdjs.MoonCode.GDGameJolt_9595ButtonObjects3= [];
gdjs.MoonCode.GDGameJolt_9595ButtonObjects4= [];
gdjs.MoonCode.GDGameJolt_9595ButtonObjects5= [];
gdjs.MoonCode.GDGameJolt_9595LogoObjects1= [];
gdjs.MoonCode.GDGameJolt_9595LogoObjects2= [];
gdjs.MoonCode.GDGameJolt_9595LogoObjects3= [];
gdjs.MoonCode.GDGameJolt_9595LogoObjects4= [];
gdjs.MoonCode.GDGameJolt_9595LogoObjects5= [];
gdjs.MoonCode.GDEarth_9595ButtonObjects1= [];
gdjs.MoonCode.GDEarth_9595ButtonObjects2= [];
gdjs.MoonCode.GDEarth_9595ButtonObjects3= [];
gdjs.MoonCode.GDEarth_9595ButtonObjects4= [];
gdjs.MoonCode.GDEarth_9595ButtonObjects5= [];
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1= [];
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects2= [];
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects3= [];
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects4= [];
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects5= [];
gdjs.MoonCode.GDDogeDirect_9595LogoObjects1= [];
gdjs.MoonCode.GDDogeDirect_9595LogoObjects2= [];
gdjs.MoonCode.GDDogeDirect_9595LogoObjects3= [];
gdjs.MoonCode.GDDogeDirect_9595LogoObjects4= [];
gdjs.MoonCode.GDDogeDirect_9595LogoObjects5= [];
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects1= [];
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2= [];
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects3= [];
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects4= [];
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects5= [];
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects1= [];
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects2= [];
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects3= [];
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects4= [];
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects5= [];
gdjs.MoonCode.GDMoon_9595ButtonObjects1= [];
gdjs.MoonCode.GDMoon_9595ButtonObjects2= [];
gdjs.MoonCode.GDMoon_9595ButtonObjects3= [];
gdjs.MoonCode.GDMoon_9595ButtonObjects4= [];
gdjs.MoonCode.GDMoon_9595ButtonObjects5= [];
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1= [];
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects2= [];
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects3= [];
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects4= [];
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects5= [];
gdjs.MoonCode.GDVolume_9595IconObjects1= [];
gdjs.MoonCode.GDVolume_9595IconObjects2= [];
gdjs.MoonCode.GDVolume_9595IconObjects3= [];
gdjs.MoonCode.GDVolume_9595IconObjects4= [];
gdjs.MoonCode.GDVolume_9595IconObjects5= [];
gdjs.MoonCode.GDMarker2Objects1= [];
gdjs.MoonCode.GDMarker2Objects2= [];
gdjs.MoonCode.GDMarker2Objects3= [];
gdjs.MoonCode.GDMarker2Objects4= [];
gdjs.MoonCode.GDMarker2Objects5= [];
gdjs.MoonCode.GDPlus1Objects1= [];
gdjs.MoonCode.GDPlus1Objects2= [];
gdjs.MoonCode.GDPlus1Objects3= [];
gdjs.MoonCode.GDPlus1Objects4= [];
gdjs.MoonCode.GDPlus1Objects5= [];
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects1= [];
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2= [];
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects3= [];
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects4= [];
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5= [];
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects1= [];
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2= [];
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects3= [];
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects4= [];
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5= [];
gdjs.MoonCode.GDMoon_9595ShibeObjects1= [];
gdjs.MoonCode.GDMoon_9595ShibeObjects2= [];
gdjs.MoonCode.GDMoon_9595ShibeObjects3= [];
gdjs.MoonCode.GDMoon_9595ShibeObjects4= [];
gdjs.MoonCode.GDMoon_9595ShibeObjects5= [];
gdjs.MoonCode.GDShibeObjects1= [];
gdjs.MoonCode.GDShibeObjects2= [];
gdjs.MoonCode.GDShibeObjects3= [];
gdjs.MoonCode.GDShibeObjects4= [];
gdjs.MoonCode.GDShibeObjects5= [];
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects1= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects3= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects4= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects5= [];
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects1= [];
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects2= [];
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects3= [];
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects4= [];
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects5= [];
gdjs.MoonCode.GDDoge_9595CarObjects1= [];
gdjs.MoonCode.GDDoge_9595CarObjects2= [];
gdjs.MoonCode.GDDoge_9595CarObjects3= [];
gdjs.MoonCode.GDDoge_9595CarObjects4= [];
gdjs.MoonCode.GDDoge_9595CarObjects5= [];
gdjs.MoonCode.GDDogeCarObjects1= [];
gdjs.MoonCode.GDDogeCarObjects2= [];
gdjs.MoonCode.GDDogeCarObjects3= [];
gdjs.MoonCode.GDDogeCarObjects4= [];
gdjs.MoonCode.GDDogeCarObjects5= [];
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDDogeCar_9595DPSObjects1= [];
gdjs.MoonCode.GDDogeCar_9595DPSObjects2= [];
gdjs.MoonCode.GDDogeCar_9595DPSObjects3= [];
gdjs.MoonCode.GDDogeCar_9595DPSObjects4= [];
gdjs.MoonCode.GDDogeCar_9595DPSObjects5= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects1= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects3= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects4= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects5= [];
gdjs.MoonCode.GDSuitShibeObjects1= [];
gdjs.MoonCode.GDSuitShibeObjects2= [];
gdjs.MoonCode.GDSuitShibeObjects3= [];
gdjs.MoonCode.GDSuitShibeObjects4= [];
gdjs.MoonCode.GDSuitShibeObjects5= [];
gdjs.MoonCode.GDSuit_9595ShibeObjects1= [];
gdjs.MoonCode.GDSuit_9595ShibeObjects2= [];
gdjs.MoonCode.GDSuit_9595ShibeObjects3= [];
gdjs.MoonCode.GDSuit_9595ShibeObjects4= [];
gdjs.MoonCode.GDSuit_9595ShibeObjects5= [];
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects1= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects3= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects4= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects5= [];
gdjs.MoonCode.GDSuitShibe_9595DPSObjects1= [];
gdjs.MoonCode.GDSuitShibe_9595DPSObjects2= [];
gdjs.MoonCode.GDSuitShibe_9595DPSObjects3= [];
gdjs.MoonCode.GDSuitShibe_9595DPSObjects4= [];
gdjs.MoonCode.GDSuitShibe_9595DPSObjects5= [];
gdjs.MoonCode.GDLander_9595ShibeObjects1= [];
gdjs.MoonCode.GDLander_9595ShibeObjects2= [];
gdjs.MoonCode.GDLander_9595ShibeObjects3= [];
gdjs.MoonCode.GDLander_9595ShibeObjects4= [];
gdjs.MoonCode.GDLander_9595ShibeObjects5= [];
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects1= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects3= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects4= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects5= [];
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects1= [];
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects2= [];
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects3= [];
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects4= [];
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects5= [];
gdjs.MoonCode.GDLanderShibeObjects1= [];
gdjs.MoonCode.GDLanderShibeObjects2= [];
gdjs.MoonCode.GDLanderShibeObjects3= [];
gdjs.MoonCode.GDLanderShibeObjects4= [];
gdjs.MoonCode.GDLanderShibeObjects5= [];
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects1= [];
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects2= [];
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects3= [];
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects4= [];
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects5= [];
gdjs.MoonCode.GDUpgradeUIObjects1= [];
gdjs.MoonCode.GDUpgradeUIObjects2= [];
gdjs.MoonCode.GDUpgradeUIObjects3= [];
gdjs.MoonCode.GDUpgradeUIObjects4= [];
gdjs.MoonCode.GDUpgradeUIObjects5= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects1= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects3= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects4= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects5= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects1= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects3= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects4= [];
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects5= [];
gdjs.MoonCode.GDLevel_95952Objects1= [];
gdjs.MoonCode.GDLevel_95952Objects2= [];
gdjs.MoonCode.GDLevel_95952Objects3= [];
gdjs.MoonCode.GDLevel_95952Objects4= [];
gdjs.MoonCode.GDLevel_95952Objects5= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects1= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects2= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects3= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects4= [];
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects5= [];
gdjs.MoonCode.GDLevel_95952_9595IdleObjects1= [];
gdjs.MoonCode.GDLevel_95952_9595IdleObjects2= [];
gdjs.MoonCode.GDLevel_95952_9595IdleObjects3= [];
gdjs.MoonCode.GDLevel_95952_9595IdleObjects4= [];
gdjs.MoonCode.GDLevel_95952_9595IdleObjects5= [];
gdjs.MoonCode.GDLevel_95952_9595ActionObjects1= [];
gdjs.MoonCode.GDLevel_95952_9595ActionObjects2= [];
gdjs.MoonCode.GDLevel_95952_9595ActionObjects3= [];
gdjs.MoonCode.GDLevel_95952_9595ActionObjects4= [];
gdjs.MoonCode.GDLevel_95952_9595ActionObjects5= [];
gdjs.MoonCode.GDMars_9595BaseObjects1= [];
gdjs.MoonCode.GDMars_9595BaseObjects2= [];
gdjs.MoonCode.GDMars_9595BaseObjects3= [];
gdjs.MoonCode.GDMars_9595BaseObjects4= [];
gdjs.MoonCode.GDMars_9595BaseObjects5= [];
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects1= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects3= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects4= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects5= [];
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects1= [];
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects2= [];
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects3= [];
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects4= [];
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects5= [];
gdjs.MoonCode.GDMars_9595RocketObjects1= [];
gdjs.MoonCode.GDMars_9595RocketObjects2= [];
gdjs.MoonCode.GDMars_9595RocketObjects3= [];
gdjs.MoonCode.GDMars_9595RocketObjects4= [];
gdjs.MoonCode.GDMars_9595RocketObjects5= [];
gdjs.MoonCode.GDMarsRocketObjects1= [];
gdjs.MoonCode.GDMarsRocketObjects2= [];
gdjs.MoonCode.GDMarsRocketObjects3= [];
gdjs.MoonCode.GDMarsRocketObjects4= [];
gdjs.MoonCode.GDMarsRocketObjects5= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects3= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects4= [];
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects5= [];
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects1= [];
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects2= [];
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects3= [];
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects4= [];
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects5= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects1= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects3= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects4= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects5= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects1= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects3= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects4= [];
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects5= [];
gdjs.MoonCode.GDLevel_95953_9595IdleObjects1= [];
gdjs.MoonCode.GDLevel_95953_9595IdleObjects2= [];
gdjs.MoonCode.GDLevel_95953_9595IdleObjects3= [];
gdjs.MoonCode.GDLevel_95953_9595IdleObjects4= [];
gdjs.MoonCode.GDLevel_95953_9595IdleObjects5= [];
gdjs.MoonCode.GDLevel_95953_9595ActionObjects1= [];
gdjs.MoonCode.GDLevel_95953_9595ActionObjects2= [];
gdjs.MoonCode.GDLevel_95953_9595ActionObjects3= [];
gdjs.MoonCode.GDLevel_95953_9595ActionObjects4= [];
gdjs.MoonCode.GDLevel_95953_9595ActionObjects5= [];
gdjs.MoonCode.GDLevel_95953Objects1= [];
gdjs.MoonCode.GDLevel_95953Objects2= [];
gdjs.MoonCode.GDLevel_95953Objects3= [];
gdjs.MoonCode.GDLevel_95953Objects4= [];
gdjs.MoonCode.GDLevel_95953Objects5= [];
gdjs.MoonCode.GDMarsBaseObjects1= [];
gdjs.MoonCode.GDMarsBaseObjects2= [];
gdjs.MoonCode.GDMarsBaseObjects3= [];
gdjs.MoonCode.GDMarsBaseObjects4= [];
gdjs.MoonCode.GDMarsBaseObjects5= [];
gdjs.MoonCode.GDMusic_9595ToggleObjects1= [];
gdjs.MoonCode.GDMusic_9595ToggleObjects2= [];
gdjs.MoonCode.GDMusic_9595ToggleObjects3= [];
gdjs.MoonCode.GDMusic_9595ToggleObjects4= [];
gdjs.MoonCode.GDMusic_9595ToggleObjects5= [];
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects1= [];
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects2= [];
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects3= [];
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects4= [];
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects5= [];
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects1= [];
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects2= [];
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects3= [];
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects4= [];
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects5= [];
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects1= [];
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects2= [];
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects3= [];
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects4= [];
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects5= [];
gdjs.MoonCode.GDSettings_9595TextObjects1= [];
gdjs.MoonCode.GDSettings_9595TextObjects2= [];
gdjs.MoonCode.GDSettings_9595TextObjects3= [];
gdjs.MoonCode.GDSettings_9595TextObjects4= [];
gdjs.MoonCode.GDSettings_9595TextObjects5= [];
gdjs.MoonCode.GDVolume_9595TextObjects1= [];
gdjs.MoonCode.GDVolume_9595TextObjects2= [];
gdjs.MoonCode.GDVolume_9595TextObjects3= [];
gdjs.MoonCode.GDVolume_9595TextObjects4= [];
gdjs.MoonCode.GDVolume_9595TextObjects5= [];
gdjs.MoonCode.GDSettings_9595ButtonObjects1= [];
gdjs.MoonCode.GDSettings_9595ButtonObjects2= [];
gdjs.MoonCode.GDSettings_9595ButtonObjects3= [];
gdjs.MoonCode.GDSettings_9595ButtonObjects4= [];
gdjs.MoonCode.GDSettings_9595ButtonObjects5= [];
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects1= [];
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects2= [];
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects3= [];
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects4= [];
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects5= [];
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects1= [];
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2= [];
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects3= [];
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects4= [];
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects5= [];
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects1= [];
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2= [];
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects3= [];
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects4= [];
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects5= [];
gdjs.MoonCode.GDSquareWhiteSliderObjects1= [];
gdjs.MoonCode.GDSquareWhiteSliderObjects2= [];
gdjs.MoonCode.GDSquareWhiteSliderObjects3= [];
gdjs.MoonCode.GDSquareWhiteSliderObjects4= [];
gdjs.MoonCode.GDSquareWhiteSliderObjects5= [];
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects1= [];
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects2= [];
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects3= [];
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects4= [];
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects5= [];
gdjs.MoonCode.GDEarth_9595UIObjects1= [];
gdjs.MoonCode.GDEarth_9595UIObjects2= [];
gdjs.MoonCode.GDEarth_9595UIObjects3= [];
gdjs.MoonCode.GDEarth_9595UIObjects4= [];
gdjs.MoonCode.GDEarth_9595UIObjects5= [];
gdjs.MoonCode.GDSettings_9595Button2Objects1= [];
gdjs.MoonCode.GDSettings_9595Button2Objects2= [];
gdjs.MoonCode.GDSettings_9595Button2Objects3= [];
gdjs.MoonCode.GDSettings_9595Button2Objects4= [];
gdjs.MoonCode.GDSettings_9595Button2Objects5= [];
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects1= [];
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects2= [];
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects3= [];
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects4= [];
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects5= [];
gdjs.MoonCode.GDLeaderboards_9595Button2Objects1= [];
gdjs.MoonCode.GDLeaderboards_9595Button2Objects2= [];
gdjs.MoonCode.GDLeaderboards_9595Button2Objects3= [];
gdjs.MoonCode.GDLeaderboards_9595Button2Objects4= [];
gdjs.MoonCode.GDLeaderboards_9595Button2Objects5= [];


gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDTip_95959595Me_95959595TextObjects2Objects = Hashtable.newFrom({"Tip_Me_Text": gdjs.MoonCode.GDTip_9595Me_9595TextObjects2});
gdjs.MoonCode.asyncCallback16223668 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Tip_Me_Text"), gdjs.MoonCode.GDTip_9595Me_9595TextObjects4);

{for(var i = 0, len = gdjs.MoonCode.GDTip_9595Me_9595TextObjects4.length ;i < len;++i) {
    gdjs.MoonCode.GDTip_9595Me_9595TextObjects4[i].deleteFromScene(runtimeScene);
}
}}
gdjs.MoonCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDTip_9595Me_9595TextObjects3) asyncObjectsList.addObject("Tip_Me_Text", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.5), (runtimeScene) => (gdjs.MoonCode.asyncCallback16223668(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16223516 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Tip_Me_Text"), gdjs.MoonCode.GDTip_9595Me_9595TextObjects3);

{for(var i = 0, len = gdjs.MoonCode.GDTip_9595Me_9595TextObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDTip_9595Me_9595TextObjects3[i].getBehavior("Opacity").setOpacity(gdjs.MoonCode.GDTip_9595Me_9595TextObjects3[i].getBehavior("Opacity").getOpacity() - (120));
}
}
{ //Subevents
gdjs.MoonCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MoonCode.eventsList1 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDTip_9595Me_9595TextObjects2) asyncObjectsList.addObject("Tip_Me_Text", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.5), (runtimeScene) => (gdjs.MoonCode.asyncCallback16223516(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.eventsList2 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Controller_Indicator_Text"), gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2);
gdjs.copyArray(runtimeScene.getObjects("Discord_Button"), gdjs.MoonCode.GDDiscord_9595ButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("DogeButton"), gdjs.MoonCode.GDDogeButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("DogeDirect_Button"), gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Earth_Button_Function"), gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects2);
gdjs.copyArray(runtimeScene.getObjects("GameJolt_Button"), gdjs.MoonCode.GDGameJolt_9595ButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Itch_IO_Button"), gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_3"), gdjs.MoonCode.GDLevel_95953Objects2);
gdjs.copyArray(runtimeScene.getObjects("Level_3_Action"), gdjs.MoonCode.GDLevel_95953_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects2);
gdjs.copyArray(runtimeScene.getObjects("Marker2"), gdjs.MoonCode.GDMarker2Objects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_3_Alt_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_3_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2);
gdjs.copyArray(runtimeScene.getObjects("Reddit_Button"), gdjs.MoonCode.GDReddit_9595ButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("SteamGroup_Button"), gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Tip_Button"), gdjs.MoonCode.GDTip_9595ButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Xbox_Control_Indicator"), gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDReddit_9595ButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDReddit_9595ButtonObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDDiscord_9595ButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDDiscord_9595ButtonObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDGameJolt_9595ButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDGameJolt_9595ButtonObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDDogeButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDDogeButtonObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMarker2Objects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMarker2Objects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2[i].hide();
}
}{/* Unknown object - skipped. */}{/* Unknown object - skipped. */}{/* Unknown object - skipped. */}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953Objects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953Objects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595ActionObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDTip_9595ButtonObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDTip_9595ButtonObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects2[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Reddit_Button"), gdjs.MoonCode.GDReddit_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDReddit_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDReddit_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDReddit_9595ButtonObjects2[k] = gdjs.MoonCode.GDReddit_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDReddit_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.window.openURL("https://www.reddit.com/r/DogeMiner/", runtimeScene);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Discord_Button"), gdjs.MoonCode.GDDiscord_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDDiscord_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDDiscord_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDDiscord_9595ButtonObjects2[k] = gdjs.MoonCode.GDDiscord_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDDiscord_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.window.openURL("https://discord.gg/KPAjHmtD", runtimeScene);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("SteamGroup_Button"), gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2[k] = gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.window.openURL("https://steamcommunity.com/groups/dogeminergame", runtimeScene);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Itch_IO_Button"), gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2[k] = gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.window.openURL("https://babylion122.itch.io/dogeminer-ce", runtimeScene);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("GameJolt_Button"), gdjs.MoonCode.GDGameJolt_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDGameJolt_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDGameJolt_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDGameJolt_9595ButtonObjects2[k] = gdjs.MoonCode.GDGameJolt_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDGameJolt_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.window.openURL("https://gamejolt.com/c/dogeminer-r52saz", runtimeScene);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("DogeDirect_Button"), gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2[k] = gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.window.openURL("https://sites.google.com/view/dogedirect", runtimeScene);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Tip_Button"), gdjs.MoonCode.GDTip_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDTip_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDTip_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDTip_9595ButtonObjects2[k] = gdjs.MoonCode.GDTip_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDTip_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16222972);
}
}
if (isConditionTrue_0) {
gdjs.MoonCode.GDTip_9595Me_9595TextObjects2.length = 0;

{gdjs.evtsExt__Clipboard__WriteText.func(runtimeScene, "DNs2bULYXKuvAhrZhpn5mzP7aKc1bNbJib", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDTip_95959595Me_95959595TextObjects2Objects, 440, 390, "Things that need to be on top of text");
}
{ //Subevents
gdjs.MoonCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "o");
if (isConditionTrue_1) {
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "g");
if (isConditionTrue_1) {
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "e");
}
}
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16225636);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.window.openURL("https://dogeminer.se/", runtimeScene);
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Earth_Button_Function"), gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1.length;i<l;++i) {
    if ( gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1[k] = gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1[i];
        ++k;
    }
}
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Earth", false);
}}

}


};gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDPlus1Objects3Objects = Hashtable.newFrom({"Plus1": gdjs.MoonCode.GDPlus1Objects3});
gdjs.MoonCode.asyncCallback16228532 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(runtimeScene.getObjects("Controller_Indicator_Text"), gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5);
gdjs.copyArray(asyncObjectsList.getObjects("Plus1"), gdjs.MoonCode.GDPlus1Objects5);

gdjs.copyArray(runtimeScene.getObjects("Xbox_Control_Indicator"), gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5);
{for(var i = 0, len = gdjs.MoonCode.GDPlus1Objects5.length ;i < len;++i) {
    gdjs.MoonCode.GDPlus1Objects5[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5.length ;i < len;++i) {
    gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5.length ;i < len;++i) {
    gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5[i].hide();
}
}}
gdjs.MoonCode.eventsList3 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDPlus1Objects4) asyncObjectsList.addObject("Plus1", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.25), (runtimeScene) => (gdjs.MoonCode.asyncCallback16228532(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16228660 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Plus1"), gdjs.MoonCode.GDPlus1Objects4);

{for(var i = 0, len = gdjs.MoonCode.GDPlus1Objects4.length ;i < len;++i) {
    gdjs.MoonCode.GDPlus1Objects4[i].getBehavior("Opacity").setOpacity(gdjs.MoonCode.GDPlus1Objects4[i].getBehavior("Opacity").getOpacity() - (50));
}
}
{ //Subevents
gdjs.MoonCode.eventsList3(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MoonCode.eventsList4 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDPlus1Objects3) asyncObjectsList.addObject("Plus1", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.25), (runtimeScene) => (gdjs.MoonCode.asyncCallback16228660(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16227892 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Moon_Level_1_PreAction"), gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3);

gdjs.MoonCode.GDPlus1Objects3.length = 0;

{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3[i].hide(false);
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Strength")));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDPlus1Objects3Objects, gdjs.evtTools.input.getCursorX(runtimeScene, "Things that need to be on top of text", 0), gdjs.evtTools.input.getCursorY(runtimeScene, "Things that need to be on top of text", 0), "Things that need to be on top of text");
}
{ //Subevents
gdjs.MoonCode.eventsList4(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MoonCode.eventsList5 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2) asyncObjectsList.addObject("Moon_Level_1_Action", obj);
for (const obj of gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2) asyncObjectsList.addObject("Moon_Level_1_PreAction", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16227892(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDPlus1Objects3Objects = Hashtable.newFrom({"Plus1": gdjs.MoonCode.GDPlus1Objects3});
gdjs.MoonCode.asyncCallback16231404 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(runtimeScene.getObjects("Controller_Indicator_Text"), gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5);
gdjs.copyArray(asyncObjectsList.getObjects("Plus1"), gdjs.MoonCode.GDPlus1Objects5);

gdjs.copyArray(runtimeScene.getObjects("Xbox_Control_Indicator"), gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5);
{for(var i = 0, len = gdjs.MoonCode.GDPlus1Objects5.length ;i < len;++i) {
    gdjs.MoonCode.GDPlus1Objects5[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5.length ;i < len;++i) {
    gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5.length ;i < len;++i) {
    gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5[i].hide();
}
}}
gdjs.MoonCode.eventsList6 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDPlus1Objects4) asyncObjectsList.addObject("Plus1", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.25), (runtimeScene) => (gdjs.MoonCode.asyncCallback16231404(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16231476 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Plus1"), gdjs.MoonCode.GDPlus1Objects4);

{for(var i = 0, len = gdjs.MoonCode.GDPlus1Objects4.length ;i < len;++i) {
    gdjs.MoonCode.GDPlus1Objects4[i].getBehavior("Opacity").setOpacity(gdjs.MoonCode.GDPlus1Objects4[i].getBehavior("Opacity").getOpacity() - (50));
}
}
{ //Subevents
gdjs.MoonCode.eventsList6(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MoonCode.eventsList7 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDPlus1Objects3) asyncObjectsList.addObject("Plus1", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.25), (runtimeScene) => (gdjs.MoonCode.asyncCallback16231476(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16230972 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Moon_Level_1_PreAction"), gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3);

gdjs.MoonCode.GDPlus1Objects3.length = 0;

{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3[i].hide(false);
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Strength")));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDPlus1Objects3Objects, gdjs.randomFloatInRange(560, 700), gdjs.randomFloatInRange(670, 770), "Things that need to be on top of text");
}
{ //Subevents
gdjs.MoonCode.eventsList7(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MoonCode.eventsList8 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2) asyncObjectsList.addObject("Moon_Level_1_Action", obj);
for (const obj of gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2) asyncObjectsList.addObject("Moon_Level_1_PreAction", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16230972(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16234692 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects3);

{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects3[i].hide(false);
}
}}
gdjs.MoonCode.eventsList9 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDLevel_95952_9595ActionObjects2) asyncObjectsList.addObject("Level_2_Action", obj);
for (const obj of gdjs.MoonCode.GDLevel_95952_9595IdleObjects2) asyncObjectsList.addObject("Level_2_Idle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16234692(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16236212 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Level_3_Action"), gdjs.MoonCode.GDLevel_95953_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects3);

{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects3[i].hide(false);
}
}}
gdjs.MoonCode.eventsList10 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDLevel_95953_9595ActionObjects2) asyncObjectsList.addObject("Level_3_Action", obj);
for (const obj of gdjs.MoonCode.GDLevel_95953_9595IdleObjects2) asyncObjectsList.addObject("Level_3_Idle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16236212(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16237892 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects3);

{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects3[i].hide(false);
}
}}
gdjs.MoonCode.eventsList11 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDLevel_95952_9595ActionObjects2) asyncObjectsList.addObject("Level_2_Action", obj);
for (const obj of gdjs.MoonCode.GDLevel_95952_9595IdleObjects2) asyncObjectsList.addObject("Level_2_Idle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16237892(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16239388 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Level_3_Action"), gdjs.MoonCode.GDLevel_95953_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects3);

{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects3[i].hide(false);
}
}}
gdjs.MoonCode.eventsList12 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDLevel_95953_9595ActionObjects2) asyncObjectsList.addObject("Level_3_Action", obj);
for (const obj of gdjs.MoonCode.GDLevel_95953_9595IdleObjects2) asyncObjectsList.addObject("Level_3_Idle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16239388(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDPlus1Objects3Objects = Hashtable.newFrom({"Plus1": gdjs.MoonCode.GDPlus1Objects3});
gdjs.MoonCode.asyncCallback16242396 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Plus1"), gdjs.MoonCode.GDPlus1Objects5);

{for(var i = 0, len = gdjs.MoonCode.GDPlus1Objects5.length ;i < len;++i) {
    gdjs.MoonCode.GDPlus1Objects5[i].deleteFromScene(runtimeScene);
}
}}
gdjs.MoonCode.eventsList13 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDPlus1Objects4) asyncObjectsList.addObject("Plus1", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.25), (runtimeScene) => (gdjs.MoonCode.asyncCallback16242396(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16241596 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Plus1"), gdjs.MoonCode.GDPlus1Objects4);

{for(var i = 0, len = gdjs.MoonCode.GDPlus1Objects4.length ;i < len;++i) {
    gdjs.MoonCode.GDPlus1Objects4[i].getBehavior("Opacity").setOpacity(gdjs.MoonCode.GDPlus1Objects4[i].getBehavior("Opacity").getOpacity() - (50));
}
}
{ //Subevents
gdjs.MoonCode.eventsList13(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MoonCode.eventsList14 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDPlus1Objects3) asyncObjectsList.addObject("Plus1", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.25), (runtimeScene) => (gdjs.MoonCode.asyncCallback16241596(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16241524 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Moon_Level_1_PreAction"), gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3);

gdjs.MoonCode.GDPlus1Objects3.length = 0;

{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3[i].hide(false);
}
}{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Strength")));
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDPlus1Objects3Objects, gdjs.randomFloatInRange(560, 700), gdjs.randomFloatInRange(670, 770), "Things that need to be on top of text");
}
{ //Subevents
gdjs.MoonCode.eventsList14(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.MoonCode.eventsList15 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2) asyncObjectsList.addObject("Moon_Level_1_Action", obj);
for (const obj of gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2) asyncObjectsList.addObject("Moon_Level_1_PreAction", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16241524(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16244140 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Level_3_Action"), gdjs.MoonCode.GDLevel_95953_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects3);

{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects3[i].hide(false);
}
}}
gdjs.MoonCode.eventsList16 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDLevel_95953_9595ActionObjects2) asyncObjectsList.addObject("Level_3_Action", obj);
for (const obj of gdjs.MoonCode.GDLevel_95953_9595IdleObjects2) asyncObjectsList.addObject("Level_3_Idle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16244140(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.asyncCallback16245516 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs.MoonCode.localVariables);
gdjs.copyArray(asyncObjectsList.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects3);

gdjs.copyArray(asyncObjectsList.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects3);

{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects3[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects3.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects3[i].hide(false);
}
}}
gdjs.MoonCode.eventsList17 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs.MoonCode.localVariables);
for (const obj of gdjs.MoonCode.GDLevel_95952_9595ActionObjects2) asyncObjectsList.addObject("Level_2_Action", obj);
for (const obj of gdjs.MoonCode.GDLevel_95952_9595IdleObjects2) asyncObjectsList.addObject("Level_2_Idle", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.01), (runtimeScene) => (gdjs.MoonCode.asyncCallback16245516(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDBuy_95959595Pickaxe_95959595Level_959595953Objects2Objects = Hashtable.newFrom({"Buy_Pickaxe_Level_3": gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2});
gdjs.MoonCode.eventsList18 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("DogeButton"), gdjs.MoonCode.GDDogeButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDDogeButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDDogeButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDDogeButtonObjects2[k] = gdjs.MoonCode.GDDogeButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDDogeButtonObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_PreAction"), gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16230508);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_PreAction"), gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList8(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("DogeCoin_Amount"), gdjs.MoonCode.GDDogeCoin_9595AmountObjects2);
gdjs.copyArray(runtimeScene.getObjects("Plus1"), gdjs.MoonCode.GDPlus1Objects2);
{for(var i = 0, len = gdjs.MoonCode.GDDogeCoin_9595AmountObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDDogeCoin_9595AmountObjects2[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.evtTools.common.floorTo(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)), 0)));
}
}{for(var i = 0, len = gdjs.MoonCode.GDPlus1Objects2.length ;i < len;++i) {
    gdjs.MoonCode.GDPlus1Objects2[i].getBehavior("Text").setText("+ " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Strength"))));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("DogeButton"), gdjs.MoonCode.GDDogeButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDDogeButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDDogeButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDDogeButtonObjects2[k] = gdjs.MoonCode.GDDogeButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDDogeButtonObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16234524);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList9(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("DogeButton"), gdjs.MoonCode.GDDogeButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDDogeButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDDogeButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDDogeButtonObjects2[k] = gdjs.MoonCode.GDDogeButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDDogeButtonObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16236020);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Level_3_Action"), gdjs.MoonCode.GDLevel_95953_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList10(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16237508);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList11(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16239004);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Level_3_Action"), gdjs.MoonCode.GDLevel_95953_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList12(runtimeScene);} //End of subevents
}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Gamepads__C_Button_pressed.func(runtimeScene, 1, "A", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16240404);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Controller_Indicator_Text"), gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_PreAction"), gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Xbox_Control_Indicator"), gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList15(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Gamepads__C_Button_pressed.func(runtimeScene, 1, "A", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16243348);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Level_3_Action"), gdjs.MoonCode.GDLevel_95953_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList16(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Gamepads__C_Button_pressed.func(runtimeScene, 1, "A", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16245036);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.MoonCode.eventsList17(runtimeScene);} //End of subevents
}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Upgrade_Button"), gdjs.MoonCode.GDUpgrade_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDUpgrade_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDUpgrade_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDUpgrade_9595ButtonObjects2[k] = gdjs.MoonCode.GDUpgrade_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDUpgrade_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Upgrade Move", gdjs.evtTools.camera.getCameraX(runtimeScene, "", 0), -(1000), "Shibes people, Shibes!", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Upgrade Move2", gdjs.evtTools.camera.getCameraX(runtimeScene, "", 0), 125, "Upgrades people, upgrades!", "linear", 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Helpers_Button"), gdjs.MoonCode.GDHelpers_9595ButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDHelpers_9595ButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDHelpers_9595ButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDHelpers_9595ButtonObjects2[k] = gdjs.MoonCode.GDHelpers_9595ButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDHelpers_9595ButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Upgrade Move", gdjs.evtTools.camera.getCameraX(runtimeScene, "", 0), 540, "Shibes people, Shibes!", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Upgrade Move2", gdjs.evtTools.camera.getCameraX(runtimeScene, "", 0), 1000, "Upgrades people, upgrades!", "linear", 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Pickaxe_Level_2"), gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[k] = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 25000;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16249700);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).sub(25000);
}{runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level").add(1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Strength").add(6);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Pickaxe_Level_3"), gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2[k] = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 300000;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16250180);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).sub(300000);
}{runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level").add(1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Strength").add(293);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16251644);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Buy_Pickaxe_Level_2"), gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2);
gdjs.copyArray(runtimeScene.getObjects("Level_2"), gdjs.MoonCode.GDLevel_95952Objects2);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects2);
gdjs.copyArray(runtimeScene.getObjects("Level_3"), gdjs.MoonCode.GDLevel_95953Objects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_Action"), gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Level_1_PreAction"), gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_2_Alt_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects2);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_2_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects2);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_3_Alt_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_3_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2);
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2.length = 0;

{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952Objects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952Objects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length ;i < len;++i) {
    gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[i].Activate(false, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953Objects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953Objects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2[i].hide(false);
}
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDBuy_95959595Pickaxe_95959595Level_959595953Objects2Objects, (( gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length === 0 ) ? 0 :gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[0].getX()), (( gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length === 0 ) ? 0 :gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[0].getY()), "Upgrades people, upgrades!");
}{for(var i = 0, len = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length ;i < len;++i) {
    gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[i].hide();
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects2[i].hide(false);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16255156);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Buy_Pickaxe_Level_3"), gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects1);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Action"), gdjs.MoonCode.GDLevel_95952_9595ActionObjects1);
gdjs.copyArray(runtimeScene.getObjects("Level_2_Idle"), gdjs.MoonCode.GDLevel_95952_9595IdleObjects1);
gdjs.copyArray(runtimeScene.getObjects("Level_3"), gdjs.MoonCode.GDLevel_95953Objects1);
gdjs.copyArray(runtimeScene.getObjects("Level_3_Idle"), gdjs.MoonCode.GDLevel_95953_9595IdleObjects1);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_3_Alt_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects1);
gdjs.copyArray(runtimeScene.getObjects("Pickaxe_Level_3_Text"), gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects1);
{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects1.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects1.length ;i < len;++i) {
    gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953Objects1.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects1.length ;i < len;++i) {
    gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595IdleObjects1.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595IdleObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95952_9595ActionObjects1.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95952_9595ActionObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.MoonCode.GDLevel_95953_9595IdleObjects1.length ;i < len;++i) {
    gdjs.MoonCode.GDLevel_95953_9595IdleObjects1[i].hide(false);
}
}}

}


};gdjs.MoonCode.eventsList19 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("DPS_Amount"), gdjs.MoonCode.GDDPS_9595AmountObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDDPS_9595AmountObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDDPS_9595AmountObjects2[i].getBehavior("Text").setText(gdjs.evtTools.common.toString((gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade1").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade1").getChild("NumberBought"))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade2").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade2").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade3").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade3").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade4").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade4").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade5").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade5").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade6").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade6").getChild("NumberBought")))) + ((gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("NumberBought"))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("NumberBought")))) + (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("DPS")) * (gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("NumberBought")))))))));
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("DogeCar_DPS"), gdjs.MoonCode.GDDogeCar_9595DPSObjects2);
gdjs.copyArray(runtimeScene.getObjects("Lander_Shibe_DPS"), gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects2);
gdjs.copyArray(runtimeScene.getObjects("Mars_Base_DPS"), gdjs.MoonCode.GDMars_9595Base_9595DPSObjects2);
gdjs.copyArray(runtimeScene.getObjects("Mars_Rocket_DPS"), gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects2);
gdjs.copyArray(runtimeScene.getObjects("Moon_Shibe_DPS"), gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects2);
gdjs.copyArray(runtimeScene.getObjects("SuitShibe_DPS"), gdjs.MoonCode.GDSuitShibe_9595DPSObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects2[i].getBehavior("Text").setText("DPS: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("DPS"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDDogeCar_9595DPSObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDDogeCar_9595DPSObjects2[i].getBehavior("Text").setText("DPS: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("DPS"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDSuitShibe_9595DPSObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDSuitShibe_9595DPSObjects2[i].getBehavior("Text").setText("DPS: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("DPS"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects2[i].getBehavior("Text").setText("DPS: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("DPS"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDMars_9595Base_9595DPSObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMars_9595Base_9595DPSObjects2[i].getBehavior("Text").setText("DPS: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("DPS"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects2[i].getBehavior("Text").setText("DPS: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("DPS"))));
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("DogeCar"), gdjs.MoonCode.GDDogeCarObjects2);
gdjs.copyArray(runtimeScene.getObjects("Lander_Shibe"), gdjs.MoonCode.GDLander_9595ShibeObjects2);
gdjs.copyArray(runtimeScene.getObjects("MarsRocket"), gdjs.MoonCode.GDMarsRocketObjects2);
gdjs.copyArray(runtimeScene.getObjects("Mars_Base"), gdjs.MoonCode.GDMars_9595BaseObjects2);
gdjs.copyArray(runtimeScene.getObjects("Shibe"), gdjs.MoonCode.GDShibeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Suit_Shibe"), gdjs.MoonCode.GDSuit_9595ShibeObjects2);
{for(var i = 0, len = gdjs.MoonCode.GDShibeObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDShibeObjects2[i].getBehavior("Text").setText("Moon Shibe\nPrice: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDMarsRocketObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMarsRocketObjects2[i].getBehavior("Text").setText("Mars Rocket\nPrice: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Cost"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDMars_9595BaseObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDMars_9595BaseObjects2[i].getBehavior("Text").setText("Mars Base\nPrice: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Cost"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDDogeCarObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDDogeCarObjects2[i].getBehavior("Text").setText("Doge Car\nPrice: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Cost"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDSuit_9595ShibeObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDSuit_9595ShibeObjects2[i].getBehavior("Text").setText("Suit Shibe Price: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Cost"))));
}
}{for(var i = 0, len = gdjs.MoonCode.GDLander_9595ShibeObjects2.length ;i < len;++i) {
    gdjs.MoonCode.GDLander_9595ShibeObjects2[i].getBehavior("Text").setText("Lander Shibe \nPrice: " + gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Cost"))));
}
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Mining_Shibe_"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost"));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16265188);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__DogeCar"), gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Cost"));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16266212);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__SuitShibe"), gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Cost"));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16268324);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Lander_Shibe"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Cost"));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16269908);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Mars_Base"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Cost"));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16271532);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Mars_Rocket"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Cost"));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16273100);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Pickaxe_Level_2"), gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[k] = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 25000;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16274564);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Pickaxe_Level_3"), gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2);
gdjs.copyArray(runtimeScene.getObjects("Buy_Sound_Toggle"), gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2[i].IsPressed((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2[k] = gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= 125000;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16275860);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "ka-ching.mp3", false, 100, 1);
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Leaderboards_Button2"), gdjs.MoonCode.GDLeaderboards_9595Button2Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDLeaderboards_9595Button2Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDLeaderboards_9595Button2Objects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDLeaderboards_9595Button2Objects2[k] = gdjs.MoonCode.GDLeaderboards_9595Button2Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDLeaderboards_9595Button2Objects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Leaderboards", false);
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Music_Toggle"), gdjs.MoonCode.GDMusic_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDMusic_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDMusic_9595ToggleObjects2[i].HasJustBeenUnchecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDMusic_9595ToggleObjects2[k] = gdjs.MoonCode.GDMusic_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDMusic_9595ToggleObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.stopSoundOnChannel(runtimeScene, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("DogeButton"), gdjs.MoonCode.GDDogeButtonObjects2);
gdjs.copyArray(runtimeScene.getObjects("Hit_Sound_Toggle"), gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDDogeButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDDogeButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDDogeButtonObjects2[k] = gdjs.MoonCode.GDDogeButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDDogeButtonObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "stone-breaking-made-with-Voicemod.mp3", false, 500, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Hit_Sound_Toggle"), gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16280444);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "stone-breaking-made-with-Voicemod.mp3", false, 500, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Hit_Sound_Toggle"), gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtsExt__Gamepads__C_Button_pressed.func(runtimeScene, 1, "A", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_1 = true;
        gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[k] = gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2.length = k;
isConditionTrue_0 = isConditionTrue_1;
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16281820);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSound(runtimeScene, "stone-breaking-made-with-Voicemod.mp3", false, 500, 1);
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SquareWhiteSlider"), gdjs.MoonCode.GDSquareWhiteSliderObjects2);
{gdjs.evtTools.sound.setGlobalVolume(runtimeScene, (( gdjs.MoonCode.GDSquareWhiteSliderObjects2.length === 0 ) ? 0 :gdjs.MoonCode.GDSquareWhiteSliderObjects2[0].Value((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined))));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Music_Toggle"), gdjs.MoonCode.GDMusic_9595ToggleObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDMusic_9595ToggleObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDMusic_9595ToggleObjects2[i].IsChecked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDMusic_9595ToggleObjects2[k] = gdjs.MoonCode.GDMusic_9595ToggleObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDMusic_9595ToggleObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16283076);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playSoundOnChannel(runtimeScene, "Moon.mp3", 1, true, 100, 1);
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.systemInfo.isNativeDesktopApp(runtimeScene);
isConditionTrue_0 = isConditionTrue_1;
}
}
if (isConditionTrue_0) {
{gdjs.evtsExt__DiscordRichPresence__ConnectToDiscord.func(runtimeScene, "1244384775511474257", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) >= 30;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{let isConditionTrue_1 = false;
isConditionTrue_1 = false;
isConditionTrue_1 = gdjs.evtTools.systemInfo.isNativeDesktopApp(runtimeScene);
isConditionTrue_0 = isConditionTrue_1;
}
}
if (isConditionTrue_0) {
{gdjs.evtsExt__DiscordRichPresence__UpdateRichPresence.func(runtimeScene, "Moon", gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2))) + " DogeCoin", 0, 0, "swole", "Wow.", "", "", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Settings_Button2"), gdjs.MoonCode.GDSettings_9595Button2Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDSettings_9595Button2Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDSettings_9595Button2Objects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDSettings_9595Button2Objects2[k] = gdjs.MoonCode.GDSettings_9595Button2Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDSettings_9595Button2Objects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Marker2"), gdjs.MoonCode.GDMarker2Objects2);
{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move", (( gdjs.MoonCode.GDMarker2Objects2.length === 0 ) ? 0 :gdjs.MoonCode.GDMarker2Objects2[0].getCenterXInScene()), gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Settings", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move1", 5000, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Buttons", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move2", 5000, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Logos/Images", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move3", 5000, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Text", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move4", 5000, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Doge", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move5", 5000, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Shibes people, Shibes!", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move6", 5000, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Upgrades people, upgrades!", "linear", 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Go_Back_Button2"), gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1.length;i<l;++i) {
    if ( gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1[k] = gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1[i];
        ++k;
    }
}
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move4", 960, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Doge", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move3", 960, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Text", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move2", 960, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Logos/Images", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move1", 960, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Buttons", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move", 5000, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Settings", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move5", 960, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Shibes people, Shibes!", "linear", 1);
}{gdjs.evtTools.tween.tweenCamera2(runtimeScene, "Camera Move6", 960, gdjs.evtTools.camera.getCameraY(runtimeScene, "", 0), "Upgrades people, upgrades!", "linear", 1);
}}

}


};gdjs.MoonCode.eventsList20 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade4").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16292908);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade4");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Upgrade4") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade4").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade4").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade4");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade3").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16295148);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade3");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Upgrade3") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade3").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade3").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade3");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade2").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16296892);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade2");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Upgrade2") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade2").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade2").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade2");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Upgrade1") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade1").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade1").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade1");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade1").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16299900);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade1");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Upgrade5") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade5").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade5").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade5");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade5").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16301844);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade5");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Upgrade6") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade6").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade6").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade6");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("EarthUpgrade6").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16303908);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Upgrade6");
}}

}


};gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDMoon_95959595ShibeObjects2Objects = Hashtable.newFrom({"Moon_Shibe": gdjs.MoonCode.GDMoon_9595ShibeObjects2});
gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDDoge_95959595CarObjects2Objects = Hashtable.newFrom({"Doge_Car": gdjs.MoonCode.GDDoge_9595CarObjects2});
gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDSuitShibeObjects2Objects = Hashtable.newFrom({"SuitShibe": gdjs.MoonCode.GDSuitShibeObjects2});
gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDLanderShibeObjects2Objects = Hashtable.newFrom({"LanderShibe": gdjs.MoonCode.GDLanderShibeObjects2});
gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDMarsBaseObjects2Objects = Hashtable.newFrom({"MarsBase": gdjs.MoonCode.GDMarsBaseObjects2});
gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDMars_95959595RocketObjects1Objects = Hashtable.newFrom({"Mars_Rocket": gdjs.MoonCode.GDMars_9595RocketObjects1});
gdjs.MoonCode.eventsList21 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Mining_Shibe_"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost"));
}
if (isConditionTrue_0) {
gdjs.MoonCode.GDMoon_9595ShibeObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDMoon_95959595ShibeObjects2Objects, gdjs.randomInRange(324, 670), gdjs.randomInRange(300, 480), "Logos/Images");
}{runtimeScene.getGame().getVariables().getFromIndex(2).sub(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost").setNumber(Math.round(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost")) * 1.1));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("NumberBought").add(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Timer").setNumber(1 / gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("NumberBought")));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16306868);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade1");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Moon_Helpers.MoonUpgrade1") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade1");
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__DogeCar"), gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Cost"));
}
if (isConditionTrue_0) {
gdjs.MoonCode.GDDoge_9595CarObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDDoge_95959595CarObjects2Objects, gdjs.randomInRange(800, 1100), gdjs.randomInRange(300, 480), "Logos/Images");
}{runtimeScene.getGame().getVariables().getFromIndex(2).sub(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Cost")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("NumberBought").add(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Timer").setNumber(1 / gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("NumberBought")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Cost").setNumber(Math.round(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Cost")) * 1.1));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16310028);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade2");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Moon_Helpers.MoonUpgrade2") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade2");
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__SuitShibe"), gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Cost"));
}
if (isConditionTrue_0) {
gdjs.MoonCode.GDSuitShibeObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDSuitShibeObjects2Objects, gdjs.randomInRange(500, 750), gdjs.randomInRange(800, 950), "Logos/Images");
}{runtimeScene.getGame().getVariables().getFromIndex(2).sub(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Cost")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Cost").setNumber(Math.round(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Cost")) * 1.1));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Timer").setNumber(1 / gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("NumberBought")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("NumberBought").add(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16314132);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade3");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Moon_Helpers.MoonUpgrade3") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade3");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Moon_Helpers.MoonUpgrade4") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade4");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16318548);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade4");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Lander_Shibe"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Cost"));
}
if (isConditionTrue_0) {
gdjs.MoonCode.GDLanderShibeObjects2.length = 0;

{runtimeScene.getGame().getVariables().getFromIndex(2).sub(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Cost")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Cost").setNumber(Math.round(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Cost")) * 1.1));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Timer").setNumber(1 / gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("NumberBought")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("NumberBought").add(1);
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDLanderShibeObjects2Objects, gdjs.randomInRange(800, 1100), gdjs.randomInRange(800, 950), "Logos/Images");
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Moon_Helpers.MoonUpgrade5") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade5");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16323140);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade5");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Mars_Base"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Cost"));
}
if (isConditionTrue_0) {
gdjs.MoonCode.GDMarsBaseObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDMarsBaseObjects2Objects, gdjs.randomInRange(324, 500), gdjs.randomInRange(500, 750), "Logos/Images");
}{runtimeScene.getGame().getVariables().getFromIndex(2).sub(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Cost")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Timer").setNumber(1 / gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("NumberBought")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Cost").setNumber(Math.round(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Cost")) * 1.1));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("NumberBought").add(1);
}}

}


{



}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "Moon_Helpers.MoonUpgrade6") > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Timer"));
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(2).add(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("DPS")));
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade6");
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("NumberBought")) > 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16327332);
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Moon_Helpers.MoonUpgrade6");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Buy_Button__Mars_Rocket"), gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1.length;i<l;++i) {
    if ( gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1[k] = gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1[i];
        ++k;
    }
}
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Cost"));
}
if (isConditionTrue_0) {
gdjs.MoonCode.GDMars_9595RocketObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MoonCode.mapOfGDgdjs_9546MoonCode_9546GDMars_95959595RocketObjects1Objects, gdjs.randomInRange(900, 1100), gdjs.randomInRange(500, 750), "Logos/Images");
}{runtimeScene.getGame().getVariables().getFromIndex(2).sub(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Cost")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Timer").setNumber(1 / gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("NumberBought")));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Cost").setNumber(Math.round(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Cost")) * 1.1));
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("NumberBought").add(1);
}}

}


};gdjs.MoonCode.eventsList22 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("DogeButton"), gdjs.MoonCode.GDDogeButtonObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.MoonCode.GDDogeButtonObjects2.length;i<l;++i) {
    if ( gdjs.MoonCode.GDDogeButtonObjects2[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.MoonCode.GDDogeButtonObjects2[k] = gdjs.MoonCode.GDDogeButtonObjects2[i];
        ++k;
    }
}
gdjs.MoonCode.GDDogeButtonObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.storage.writeStringInJSONFile("Currencies", "DogeCoin", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(2)));
}{gdjs.evtTools.storage.writeStringInJSONFile("Helpers", "Earth", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(1)));
}{gdjs.evtTools.storage.writeStringInJSONFile("Helpers", "Moon", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(0)));
}{gdjs.evtTools.storage.writeNumberInJSONFile("PickaxeMoon", "Strength", gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Strength")));
}{gdjs.evtTools.storage.writeNumberInJSONFile("PickaxeMoon", "Level", gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0).getChild("Level")));
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost")) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16332332);
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Cost").setNumber(10000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("DPS").setNumber(9);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade1").getChild("Timer").setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Cost").setNumber(35000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("DPS").setNumber(20);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade2").getChild("Timer").setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Cost").setNumber(50000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("DPS").setNumber(32);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade3").getChild("Timer").setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Cost").setNumber(420000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("DPS").setNumber(135);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("Timer").setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Cost").setNumber(1500000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("DPS").setNumber(6000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade5").getChild("Timer").setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Cost").setNumber(3000000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("DPS").setNumber(10000);
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade6").getChild("Timer").setNumber(1);
}}

}


};gdjs.MoonCode.eventsList23 = function(runtimeScene) {

{


gdjs.MoonCode.eventsList2(runtimeScene);
}


{


gdjs.MoonCode.eventsList18(runtimeScene);
}


{


gdjs.MoonCode.eventsList19(runtimeScene);
}


{


gdjs.MoonCode.eventsList20(runtimeScene);
}


{


gdjs.MoonCode.eventsList21(runtimeScene);
}


{


gdjs.MoonCode.eventsList22(runtimeScene);
}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("MoonUpgrade4").getChild("NumberBought")) >= 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16334524);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("Requires_Lander_Shibe"), gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects1);
{for(var i = 0, len = gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects1.length ;i < len;++i) {
    gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


};

gdjs.MoonCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects1.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects2.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects3.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects4.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Moon_9595Objects5.length = 0;
gdjs.MoonCode.GDMoon_9595UIObjects1.length = 0;
gdjs.MoonCode.GDMoon_9595UIObjects2.length = 0;
gdjs.MoonCode.GDMoon_9595UIObjects3.length = 0;
gdjs.MoonCode.GDMoon_9595UIObjects4.length = 0;
gdjs.MoonCode.GDMoon_9595UIObjects5.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects1.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects2.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects3.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects4.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595PreActionObjects5.length = 0;
gdjs.MoonCode.GDDogeButtonObjects1.length = 0;
gdjs.MoonCode.GDDogeButtonObjects2.length = 0;
gdjs.MoonCode.GDDogeButtonObjects3.length = 0;
gdjs.MoonCode.GDDogeButtonObjects4.length = 0;
gdjs.MoonCode.GDDogeButtonObjects5.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects1.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects2.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects3.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects4.length = 0;
gdjs.MoonCode.GDMoon_9595Level_95951_9595ActionObjects5.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595Objects1.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595Objects2.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595Objects3.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595Objects4.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595Objects5.length = 0;
gdjs.MoonCode.GDDogeCoin_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDDogeCoin_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDDogeCoin_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDDogeCoin_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDDogeCoin_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDDogeCoin_9595AmountObjects1.length = 0;
gdjs.MoonCode.GDDogeCoin_9595AmountObjects2.length = 0;
gdjs.MoonCode.GDDogeCoin_9595AmountObjects3.length = 0;
gdjs.MoonCode.GDDogeCoin_9595AmountObjects4.length = 0;
gdjs.MoonCode.GDDogeCoin_9595AmountObjects5.length = 0;
gdjs.MoonCode.GDDogeCoinsObjects1.length = 0;
gdjs.MoonCode.GDDogeCoinsObjects2.length = 0;
gdjs.MoonCode.GDDogeCoinsObjects3.length = 0;
gdjs.MoonCode.GDDogeCoinsObjects4.length = 0;
gdjs.MoonCode.GDDogeCoinsObjects5.length = 0;
gdjs.MoonCode.GDDPS_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDDPS_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDDPS_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDDPS_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDDPS_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDTotal_9595DPSObjects1.length = 0;
gdjs.MoonCode.GDTotal_9595DPSObjects2.length = 0;
gdjs.MoonCode.GDTotal_9595DPSObjects3.length = 0;
gdjs.MoonCode.GDTotal_9595DPSObjects4.length = 0;
gdjs.MoonCode.GDTotal_9595DPSObjects5.length = 0;
gdjs.MoonCode.GDDPS_9595AmountObjects1.length = 0;
gdjs.MoonCode.GDDPS_9595AmountObjects2.length = 0;
gdjs.MoonCode.GDDPS_9595AmountObjects3.length = 0;
gdjs.MoonCode.GDDPS_9595AmountObjects4.length = 0;
gdjs.MoonCode.GDDPS_9595AmountObjects5.length = 0;
gdjs.MoonCode.GDMoonShopUIObjects1.length = 0;
gdjs.MoonCode.GDMoonShopUIObjects2.length = 0;
gdjs.MoonCode.GDMoonShopUIObjects3.length = 0;
gdjs.MoonCode.GDMoonShopUIObjects4.length = 0;
gdjs.MoonCode.GDMoonShopUIObjects5.length = 0;
gdjs.MoonCode.GDUpgrade_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDUpgrade_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDUpgrade_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDUpgrade_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDUpgrade_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDHelpers_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDHelpers_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDHelpers_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDHelpers_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDHelpers_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDReddit_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDReddit_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDReddit_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDReddit_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDReddit_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDReddit_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDReddit_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDReddit_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDReddit_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDReddit_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDTip_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDTip_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDTip_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDTip_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDTip_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595TextObjects1.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595TextObjects2.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595TextObjects3.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595TextObjects4.length = 0;
gdjs.MoonCode.GDTip_9595Me_9595TextObjects5.length = 0;
gdjs.MoonCode.GDDiscord_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDDiscord_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDDiscord_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDDiscord_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDDiscord_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDDiscord_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDDiscord_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDDiscord_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDDiscord_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDDiscord_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDSteamGroup_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDSteamGroup_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDSteamGroup_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDSteamGroup_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDSteamGroup_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDSteamGroup_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDItch_9595IO_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDGameJolt_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDGameJolt_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDGameJolt_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDGameJolt_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDGameJolt_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDGameJolt_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDGameJolt_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDGameJolt_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDGameJolt_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDGameJolt_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDEarth_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDEarth_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDEarth_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDEarth_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDEarth_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects1.length = 0;
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects2.length = 0;
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects3.length = 0;
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects4.length = 0;
gdjs.MoonCode.GDEarth_9595Button_9595FunctionObjects5.length = 0;
gdjs.MoonCode.GDDogeDirect_9595LogoObjects1.length = 0;
gdjs.MoonCode.GDDogeDirect_9595LogoObjects2.length = 0;
gdjs.MoonCode.GDDogeDirect_9595LogoObjects3.length = 0;
gdjs.MoonCode.GDDogeDirect_9595LogoObjects4.length = 0;
gdjs.MoonCode.GDDogeDirect_9595LogoObjects5.length = 0;
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDDogeDirect_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects1.length = 0;
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects2.length = 0;
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects3.length = 0;
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects4.length = 0;
gdjs.MoonCode.GDSocial_9595Media_9595TextObjects5.length = 0;
gdjs.MoonCode.GDMoon_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDMoon_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDMoon_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDMoon_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDMoon_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects1.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects2.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects3.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects4.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595Button2Objects5.length = 0;
gdjs.MoonCode.GDVolume_9595IconObjects1.length = 0;
gdjs.MoonCode.GDVolume_9595IconObjects2.length = 0;
gdjs.MoonCode.GDVolume_9595IconObjects3.length = 0;
gdjs.MoonCode.GDVolume_9595IconObjects4.length = 0;
gdjs.MoonCode.GDVolume_9595IconObjects5.length = 0;
gdjs.MoonCode.GDMarker2Objects1.length = 0;
gdjs.MoonCode.GDMarker2Objects2.length = 0;
gdjs.MoonCode.GDMarker2Objects3.length = 0;
gdjs.MoonCode.GDMarker2Objects4.length = 0;
gdjs.MoonCode.GDMarker2Objects5.length = 0;
gdjs.MoonCode.GDPlus1Objects1.length = 0;
gdjs.MoonCode.GDPlus1Objects2.length = 0;
gdjs.MoonCode.GDPlus1Objects3.length = 0;
gdjs.MoonCode.GDPlus1Objects4.length = 0;
gdjs.MoonCode.GDPlus1Objects5.length = 0;
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects1.length = 0;
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects2.length = 0;
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects3.length = 0;
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects4.length = 0;
gdjs.MoonCode.GDXbox_9595Control_9595IndicatorObjects5.length = 0;
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects1.length = 0;
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects2.length = 0;
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects3.length = 0;
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects4.length = 0;
gdjs.MoonCode.GDController_9595Indicator_9595TextObjects5.length = 0;
gdjs.MoonCode.GDMoon_9595ShibeObjects1.length = 0;
gdjs.MoonCode.GDMoon_9595ShibeObjects2.length = 0;
gdjs.MoonCode.GDMoon_9595ShibeObjects3.length = 0;
gdjs.MoonCode.GDMoon_9595ShibeObjects4.length = 0;
gdjs.MoonCode.GDMoon_9595ShibeObjects5.length = 0;
gdjs.MoonCode.GDShibeObjects1.length = 0;
gdjs.MoonCode.GDShibeObjects2.length = 0;
gdjs.MoonCode.GDShibeObjects3.length = 0;
gdjs.MoonCode.GDShibeObjects4.length = 0;
gdjs.MoonCode.GDShibeObjects5.length = 0;
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDShibe_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects1.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects2.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects3.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects4.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mining_9595Shibe_9595Objects5.length = 0;
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects1.length = 0;
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects2.length = 0;
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects3.length = 0;
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects4.length = 0;
gdjs.MoonCode.GDMoon_9595Shibe_9595DPSObjects5.length = 0;
gdjs.MoonCode.GDDoge_9595CarObjects1.length = 0;
gdjs.MoonCode.GDDoge_9595CarObjects2.length = 0;
gdjs.MoonCode.GDDoge_9595CarObjects3.length = 0;
gdjs.MoonCode.GDDoge_9595CarObjects4.length = 0;
gdjs.MoonCode.GDDoge_9595CarObjects5.length = 0;
gdjs.MoonCode.GDDogeCarObjects1.length = 0;
gdjs.MoonCode.GDDogeCarObjects2.length = 0;
gdjs.MoonCode.GDDogeCarObjects3.length = 0;
gdjs.MoonCode.GDDogeCarObjects4.length = 0;
gdjs.MoonCode.GDDogeCarObjects5.length = 0;
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDDogeCar_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDDogeCar_9595DPSObjects1.length = 0;
gdjs.MoonCode.GDDogeCar_9595DPSObjects2.length = 0;
gdjs.MoonCode.GDDogeCar_9595DPSObjects3.length = 0;
gdjs.MoonCode.GDDogeCar_9595DPSObjects4.length = 0;
gdjs.MoonCode.GDDogeCar_9595DPSObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects1.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects2.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects3.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects4.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595DogeCarObjects5.length = 0;
gdjs.MoonCode.GDSuitShibeObjects1.length = 0;
gdjs.MoonCode.GDSuitShibeObjects2.length = 0;
gdjs.MoonCode.GDSuitShibeObjects3.length = 0;
gdjs.MoonCode.GDSuitShibeObjects4.length = 0;
gdjs.MoonCode.GDSuitShibeObjects5.length = 0;
gdjs.MoonCode.GDSuit_9595ShibeObjects1.length = 0;
gdjs.MoonCode.GDSuit_9595ShibeObjects2.length = 0;
gdjs.MoonCode.GDSuit_9595ShibeObjects3.length = 0;
gdjs.MoonCode.GDSuit_9595ShibeObjects4.length = 0;
gdjs.MoonCode.GDSuit_9595ShibeObjects5.length = 0;
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDSuitShibe_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects1.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects2.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects3.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects4.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595SuitShibeObjects5.length = 0;
gdjs.MoonCode.GDSuitShibe_9595DPSObjects1.length = 0;
gdjs.MoonCode.GDSuitShibe_9595DPSObjects2.length = 0;
gdjs.MoonCode.GDSuitShibe_9595DPSObjects3.length = 0;
gdjs.MoonCode.GDSuitShibe_9595DPSObjects4.length = 0;
gdjs.MoonCode.GDSuitShibe_9595DPSObjects5.length = 0;
gdjs.MoonCode.GDLander_9595ShibeObjects1.length = 0;
gdjs.MoonCode.GDLander_9595ShibeObjects2.length = 0;
gdjs.MoonCode.GDLander_9595ShibeObjects3.length = 0;
gdjs.MoonCode.GDLander_9595ShibeObjects4.length = 0;
gdjs.MoonCode.GDLander_9595ShibeObjects5.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects1.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects2.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects3.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects4.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Lander_9595ShibeObjects5.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects1.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects2.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects3.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects4.length = 0;
gdjs.MoonCode.GDLander_9595Shibe_9595DPSObjects5.length = 0;
gdjs.MoonCode.GDLanderShibeObjects1.length = 0;
gdjs.MoonCode.GDLanderShibeObjects2.length = 0;
gdjs.MoonCode.GDLanderShibeObjects3.length = 0;
gdjs.MoonCode.GDLanderShibeObjects4.length = 0;
gdjs.MoonCode.GDLanderShibeObjects5.length = 0;
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects1.length = 0;
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects2.length = 0;
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects3.length = 0;
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects4.length = 0;
gdjs.MoonCode.GDRequires_9595Lander_9595ShibeObjects5.length = 0;
gdjs.MoonCode.GDUpgradeUIObjects1.length = 0;
gdjs.MoonCode.GDUpgradeUIObjects2.length = 0;
gdjs.MoonCode.GDUpgradeUIObjects3.length = 0;
gdjs.MoonCode.GDUpgradeUIObjects4.length = 0;
gdjs.MoonCode.GDUpgradeUIObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects1.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects2.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects3.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects4.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95953Objects5.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects1.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects2.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects3.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects4.length = 0;
gdjs.MoonCode.GDBuy_9595Pickaxe_9595Level_95952Objects5.length = 0;
gdjs.MoonCode.GDLevel_95952Objects1.length = 0;
gdjs.MoonCode.GDLevel_95952Objects2.length = 0;
gdjs.MoonCode.GDLevel_95952Objects3.length = 0;
gdjs.MoonCode.GDLevel_95952Objects4.length = 0;
gdjs.MoonCode.GDLevel_95952Objects5.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects1.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects2.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects3.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects4.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95952_9595TextObjects5.length = 0;
gdjs.MoonCode.GDLevel_95952_9595IdleObjects1.length = 0;
gdjs.MoonCode.GDLevel_95952_9595IdleObjects2.length = 0;
gdjs.MoonCode.GDLevel_95952_9595IdleObjects3.length = 0;
gdjs.MoonCode.GDLevel_95952_9595IdleObjects4.length = 0;
gdjs.MoonCode.GDLevel_95952_9595IdleObjects5.length = 0;
gdjs.MoonCode.GDLevel_95952_9595ActionObjects1.length = 0;
gdjs.MoonCode.GDLevel_95952_9595ActionObjects2.length = 0;
gdjs.MoonCode.GDLevel_95952_9595ActionObjects3.length = 0;
gdjs.MoonCode.GDLevel_95952_9595ActionObjects4.length = 0;
gdjs.MoonCode.GDLevel_95952_9595ActionObjects5.length = 0;
gdjs.MoonCode.GDMars_9595BaseObjects1.length = 0;
gdjs.MoonCode.GDMars_9595BaseObjects2.length = 0;
gdjs.MoonCode.GDMars_9595BaseObjects3.length = 0;
gdjs.MoonCode.GDMars_9595BaseObjects4.length = 0;
gdjs.MoonCode.GDMars_9595BaseObjects5.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects1.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects2.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects3.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects4.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595BaseObjects5.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects1.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects2.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects3.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects4.length = 0;
gdjs.MoonCode.GDMars_9595Base_9595DPSObjects5.length = 0;
gdjs.MoonCode.GDMars_9595RocketObjects1.length = 0;
gdjs.MoonCode.GDMars_9595RocketObjects2.length = 0;
gdjs.MoonCode.GDMars_9595RocketObjects3.length = 0;
gdjs.MoonCode.GDMars_9595RocketObjects4.length = 0;
gdjs.MoonCode.GDMars_9595RocketObjects5.length = 0;
gdjs.MoonCode.GDMarsRocketObjects1.length = 0;
gdjs.MoonCode.GDMarsRocketObjects2.length = 0;
gdjs.MoonCode.GDMarsRocketObjects3.length = 0;
gdjs.MoonCode.GDMarsRocketObjects4.length = 0;
gdjs.MoonCode.GDMarsRocketObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects1.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects2.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects3.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects4.length = 0;
gdjs.MoonCode.GDBuy_9595Button_9595_9595Mars_9595RocketObjects5.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects1.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects2.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects3.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects4.length = 0;
gdjs.MoonCode.GDMars_9595Rocket_9595DPSObjects5.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects1.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects2.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects3.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects4.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595Alt_9595TextObjects5.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects1.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects2.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects3.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects4.length = 0;
gdjs.MoonCode.GDPickaxe_9595Level_95953_9595TextObjects5.length = 0;
gdjs.MoonCode.GDLevel_95953_9595IdleObjects1.length = 0;
gdjs.MoonCode.GDLevel_95953_9595IdleObjects2.length = 0;
gdjs.MoonCode.GDLevel_95953_9595IdleObjects3.length = 0;
gdjs.MoonCode.GDLevel_95953_9595IdleObjects4.length = 0;
gdjs.MoonCode.GDLevel_95953_9595IdleObjects5.length = 0;
gdjs.MoonCode.GDLevel_95953_9595ActionObjects1.length = 0;
gdjs.MoonCode.GDLevel_95953_9595ActionObjects2.length = 0;
gdjs.MoonCode.GDLevel_95953_9595ActionObjects3.length = 0;
gdjs.MoonCode.GDLevel_95953_9595ActionObjects4.length = 0;
gdjs.MoonCode.GDLevel_95953_9595ActionObjects5.length = 0;
gdjs.MoonCode.GDLevel_95953Objects1.length = 0;
gdjs.MoonCode.GDLevel_95953Objects2.length = 0;
gdjs.MoonCode.GDLevel_95953Objects3.length = 0;
gdjs.MoonCode.GDLevel_95953Objects4.length = 0;
gdjs.MoonCode.GDLevel_95953Objects5.length = 0;
gdjs.MoonCode.GDMarsBaseObjects1.length = 0;
gdjs.MoonCode.GDMarsBaseObjects2.length = 0;
gdjs.MoonCode.GDMarsBaseObjects3.length = 0;
gdjs.MoonCode.GDMarsBaseObjects4.length = 0;
gdjs.MoonCode.GDMarsBaseObjects5.length = 0;
gdjs.MoonCode.GDMusic_9595ToggleObjects1.length = 0;
gdjs.MoonCode.GDMusic_9595ToggleObjects2.length = 0;
gdjs.MoonCode.GDMusic_9595ToggleObjects3.length = 0;
gdjs.MoonCode.GDMusic_9595ToggleObjects4.length = 0;
gdjs.MoonCode.GDMusic_9595ToggleObjects5.length = 0;
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects1.length = 0;
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects2.length = 0;
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects3.length = 0;
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects4.length = 0;
gdjs.MoonCode.GDToggle_9595Music_9595TextObjects5.length = 0;
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects1.length = 0;
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects2.length = 0;
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects3.length = 0;
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects4.length = 0;
gdjs.MoonCode.GDToggle_9595Buy_9595Sounds_9595TextObjects5.length = 0;
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects1.length = 0;
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects2.length = 0;
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects3.length = 0;
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects4.length = 0;
gdjs.MoonCode.GDEnable_9595Rock_9595Hitting_9595TextObjects5.length = 0;
gdjs.MoonCode.GDSettings_9595TextObjects1.length = 0;
gdjs.MoonCode.GDSettings_9595TextObjects2.length = 0;
gdjs.MoonCode.GDSettings_9595TextObjects3.length = 0;
gdjs.MoonCode.GDSettings_9595TextObjects4.length = 0;
gdjs.MoonCode.GDSettings_9595TextObjects5.length = 0;
gdjs.MoonCode.GDVolume_9595TextObjects1.length = 0;
gdjs.MoonCode.GDVolume_9595TextObjects2.length = 0;
gdjs.MoonCode.GDVolume_9595TextObjects3.length = 0;
gdjs.MoonCode.GDVolume_9595TextObjects4.length = 0;
gdjs.MoonCode.GDVolume_9595TextObjects5.length = 0;
gdjs.MoonCode.GDSettings_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDSettings_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDSettings_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDSettings_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDSettings_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDGo_9595Back_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects1.length = 0;
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects2.length = 0;
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects3.length = 0;
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects4.length = 0;
gdjs.MoonCode.GDHit_9595Sound_9595ToggleObjects5.length = 0;
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects1.length = 0;
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects2.length = 0;
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects3.length = 0;
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects4.length = 0;
gdjs.MoonCode.GDBuy_9595Sound_9595ToggleObjects5.length = 0;
gdjs.MoonCode.GDSquareWhiteSliderObjects1.length = 0;
gdjs.MoonCode.GDSquareWhiteSliderObjects2.length = 0;
gdjs.MoonCode.GDSquareWhiteSliderObjects3.length = 0;
gdjs.MoonCode.GDSquareWhiteSliderObjects4.length = 0;
gdjs.MoonCode.GDSquareWhiteSliderObjects5.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects1.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects2.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects3.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects4.length = 0;
gdjs.MoonCode.GDBackground_9595_9595Earth_9595Objects5.length = 0;
gdjs.MoonCode.GDEarth_9595UIObjects1.length = 0;
gdjs.MoonCode.GDEarth_9595UIObjects2.length = 0;
gdjs.MoonCode.GDEarth_9595UIObjects3.length = 0;
gdjs.MoonCode.GDEarth_9595UIObjects4.length = 0;
gdjs.MoonCode.GDEarth_9595UIObjects5.length = 0;
gdjs.MoonCode.GDSettings_9595Button2Objects1.length = 0;
gdjs.MoonCode.GDSettings_9595Button2Objects2.length = 0;
gdjs.MoonCode.GDSettings_9595Button2Objects3.length = 0;
gdjs.MoonCode.GDSettings_9595Button2Objects4.length = 0;
gdjs.MoonCode.GDSettings_9595Button2Objects5.length = 0;
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects1.length = 0;
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects2.length = 0;
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects3.length = 0;
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects4.length = 0;
gdjs.MoonCode.GDLeaderBoards_9595ButtonObjects5.length = 0;
gdjs.MoonCode.GDLeaderboards_9595Button2Objects1.length = 0;
gdjs.MoonCode.GDLeaderboards_9595Button2Objects2.length = 0;
gdjs.MoonCode.GDLeaderboards_9595Button2Objects3.length = 0;
gdjs.MoonCode.GDLeaderboards_9595Button2Objects4.length = 0;
gdjs.MoonCode.GDLeaderboards_9595Button2Objects5.length = 0;

gdjs.MoonCode.eventsList23(runtimeScene);

return;

}

gdjs['MoonCode'] = gdjs.MoonCode;
