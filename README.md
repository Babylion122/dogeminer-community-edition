
# DogeMiner Community Edition

DogeMiner entirely reverse engineered and created in GDevelop for modability and to be able to run natively on other devices. Project files are avaliable to create mods directly in GDevelop, or use your favorite IDE with Source Code if you like.


![](https://gitlab.com/Babylion122/dogeminer-community-edition/-/raw/main/DogeMiner:CE%20Assets/dogeminer_ce_gitlab_logo.png)


## Authors

- [@babylion122](https://gitlab.com/Babylion122)


## Installation

### Linux
Download the AppImage from our official places below
[Google Drive](https://sites.google.com/view/dogedirect/dogeminer-ce-build-page)
or
[Itch.io](https://babylion122.itch.io/dogeminer-ce)

or 

[Build it Yourself](https://wiki.gdevelop.io/gdevelop5/publishing/windows-macos-linux-with-electron/#pre-requisite-download-and-install-yarn)

### Windows
Download the installer or .zip from our official places,
[Google Drive](https://sites.google.com/view/dogedirect/dogeminer-ce-build-page)
or
[Itch.io](https://babylion122.itch.io/dogeminer-ce)
, then run the .exe within the Zip file or the Installer Exe, and the game should open once finished, if not the game should be installed and you can open it from where you installed it or Windows Start.

You can also,

[Build it Yourself](https://wiki.gdevelop.io/gdevelop5/publishing/windows-macos-linux-with-electron/#pre-requisite-download-and-install-yarn)    

### MacOS
Download the .zip from our official places,
[Google Drive](https://sites.google.com/view/dogedirect/dogeminer-ce-build-page)
or
[Itch.io](https://babylion122.itch.io/dogeminer-ce)

or 

[Build it Yourself](https://wiki.gdevelop.io/gdevelop5/publishing/windows-macos-linux-with-electron/#pre-requisite-download-and-install-yarn)

### Android
Download the .apk from our official places,
[Google Drive](https://sites.google.com/view/dogedirect/dogeminer-ce-build-page)
or
[Itch.io](https://babylion122.itch.io/dogeminer-ce)

or

[Build it Yourself](https://wiki.gdevelop.io/gdevelop5/publishing/android_and_ios_with_cordova/)

### iOS
Get it on AltStore using our Source! [Don't have AltStore?](https://altstore.io/) and 
[How to add Sources](https://faq.altstore.io/how-to-use-altstore/sources)


or

Download the .ipa from our official places,
[Google Drive](https://sites.google.com/view/dogedirect/dogeminer-ce-build-page)
or
[Itch.io](https://babylion122.itch.io/dogeminer-ce)

or

[Build it Yourself](https://wiki.gdevelop.io/gdevelop5/publishing/android_and_ios_with_cordova/)

### HTML
[Play it on itch.io](https://babylion122.itch.io)

Will be on GitLab, CrazyGames, and Poki soon.
## Related

Below is what this project is based off, made by the original creator rkn who gave permission for assets to be used here. 

[DogeMiner 1](https://dogeminer.se)
and
[DogeMiner 2](https://dogeminer2.com)



## Modding

Want to mod this game? Fork it! Simple as that. Want tutorials? [We have a YouTube channel for just that!](https://www.youtube.com/channel/UCEmOpPKtJQhEZR_DHzatU7Q)